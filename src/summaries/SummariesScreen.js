import React from "react";
import Spinner from "../ubiquitous/Spinner";
import {firestore} from "../Firebase";
import EventSummary from "../data_classes/EventSummary";
import {Paper} from "@material-ui/core";
import EventIcon from '@material-ui/icons/Event';

export default class SummariesScreen extends React.Component {

    constructor(props){
        super(props);

        this.state = {
            summaries: null,
        };

    }

    componentDidMount() {
        //todo paginate
        firestore.collection('summaries').orderBy('scheduled', 'desc').get().then(snap => {
            const arr = [];
            snap.forEach(doc => {
                arr.push(new EventSummary(doc));
            });
            this.setState({summaries: arr});
        });
    }

    render() {

        return (
            <div style={{margin: 'auto', width: 842, maxWidth: '90vw'}}>
                <h1>Past event summaries</h1>
                <p>At most events, notes are taken about some remarkable points raised on the topic of discussion. Summaries of
                    what was discussed can be found here. For privacy's sake no speakers are ever named. Note that
                    these summaries are not fully-comprehensive. To get the full experience, come to the events!</p>
                {!this.state.summaries && <div >
                    <Spinner text="Loading past events..."/></div>}
                {this.state.summaries && <div >

                    {this.state.summaries.map(eventSum => {
                        return <Paper key={eventSum.id} style={{padding: 24, marginBottom: 32}}>
                            <h3>{eventSum.topic}</h3>
                            <table><tbody><tr><td><EventIcon/></td>
                                <td>{eventSum.formatScheduled()}</td></tr></tbody></table>
                            <p>{eventSum.summary.map((par, i) => {
                                return <span key={i}>{par}<br/></span>
                                })}</p>
                        </Paper>
                    })}
                </div>}
            </div>
        );
    }

}
