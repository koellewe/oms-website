import React from "react";
import './Home.css';
import EmailSubber from "./EmailSubber";
import EventOutlinedIcon from '@material-ui/icons/EventOutlined';
import EventIcon from '@material-ui/icons/Event';
import LocationOnIcon from '@material-ui/icons/LocationOn';
import CustomLogo from "./CustomLogo";
import {analytics, firestore} from "../Firebase";
import OmsocEvent from "../data_classes/OmsocEvent";
import {Link} from "react-router-dom";
import PastVoting from "../admin/PastVoting";
import CircularProgress from "@material-ui/core/CircularProgress";

export default class Home extends React.Component{

    constructor(props){
        super(props);

        this.state = {
            loadingEvent: true,
            latestEvent: null,
        };
    }

    static addHours(date, h){
        date.setTime(date.getTime() + (h*60*60*1000));
        return date;
    }

    componentDidMount() {
        //get soonest event in future, where "future" is 2 hours ago, forward
        firestore.collection('events').where('scheduledFor', '>=', Home.addHours(new Date(), -2))
            .orderBy('scheduledFor', 'asc').limit(1).get().then(snap => {
                if (!snap.empty){
                    this.setState({
                        latestEvent: new OmsocEvent(snap.docs[0])
                    });
                }
                this.setState({loadingEvent: false});
                //react should be clever enough to conditionally combine these updates
        });
        analytics.logEvent('screen_view', {'screen_name': 'Home'});
    }

    render() {
        return (<div>
            <div id="main_home">
                {/*Note: do not put anything above the logo here.*/}
                    <CustomLogo/>
                <div className="info_block">
                    <h1>Open Mind Society</h1>
                    <p style={{marginTop: '-10px'}}><em>University of Cape Town</em></p>

                    <div style={{marginTop: '64px'}}>
                        {this.state.loadingEvent && <div style={{margin: 'auto',}}><CircularProgress thickness={6} size={104} /></div>}
                        {!this.state.loadingEvent && (
                            this.state.latestEvent ?
                                <div id="next-event-block">
                                    <h2><u>{this.state.latestEvent.topic}</u></h2>
                                    <div className="flexy">
                                        <table><tbody><tr><td><EventIcon/></td>
                                            <td>{PastVoting.formatDate(this.state.latestEvent.scheduledForUnix)}</td></tr></tbody></table>
                                        <div className="flexboom"/>
                                        <table><tbody><tr><td><LocationOnIcon/></td>
                                            <td>{this.state.latestEvent.venue}</td></tr></tbody></table>
                                    </div>
                                    <p dangerouslySetInnerHTML={{__html: this.state.latestEvent.description}} />
                                </div>
                                :
                                <div >
                                    <EventOutlinedIcon fontSize="large"/>
                                    <p>Watch this space for new events.</p>
                                </div>
                        )}
                    </div>

                </div>

                <EmailSubber />

                <div style={{marginTop: '64px'}}>
                    <h2>Find us everywhere</h2>
                    <div style={{display: 'flex', flexDirection: 'row'}}>
                        <div style={{flexGrow: 1}}/>
                        <div style={{flexGrow: 1, display: 'flex', flexDirection: 'row'}}>
                            <a href="https://www.facebook.com/uctopenmindsoc" style={{flexGrow: 1}}>
                                <img src={require('../assets/icons/fb.png')} alt="OMS on Facebook"
                                     width={50} height={50}/></a>
                            <a href="https://vula.uct.ac.za/portal/site/2f91780a-1600-4d31-a759-12846c8f4616" style={{flexGrow: 1}}>
                                <img src={require('../assets/icons/vula.png')} alt="OMS on Vula"
                                     width={50} height={50}/></a>
                            <a href="https://twitter.com/mind_uct" style={{flexGrow: 1}}>
                                <img src={require('../assets/icons/twitter.png')} alt="OMS on Twitter"
                                     width={60} height={50}/></a>
                        </div>
                        <div style={{flexGrow: 1}}/>
                    </div>
                </div>

                <p id="par_queries" className="info_block">If you have any suggestions or queries, feel free
                    to <Link to="/committee">get in contact</Link> with the executive committee.</p>
            </div>
        </div>);
    }

}
