import {firestore} from "../Firebase";

export default class Topic{
    constructor(doc){
        this.id = doc.id;
        const data = doc.data();

        //compulsory
        this.title = data.title;
        this.submittedOn = data.submittedOn.seconds; //have to extract out seconds, because react state doesn't like dates

        this.suggester = data.suggester; //name can be fetched later
        this.suggesterName = '';
    }

    formatSubmittedOn(detail=false) {
        return Topic.formatDate(this.submittedOn, detail);
    }

    static formatDate(seconds, detail=false) {
        const date = new Date(seconds * 1000);

        if (detail) {
            return date.toLocaleDateString('en-ZA', {
                day: 'numeric', month: 'short', weekday: 'short'
            }) + ' @ ' + date.toLocaleTimeString('en-ZA', {hour12: true, hour:'numeric'})
        } else {
            return date.toLocaleDateString("en-ZA", {
                year: 'numeric', month: 'short', day: 'numeric'
            });
        }
    }

    getSuggesterName() {

        return new Promise((resolve, reject)=>{
            if (this.suggesterName) resolve(this.suggesterName);
            else firestore.collection('users').doc(this.suggester).get().then(doc => {
                this.suggesterName = doc.data().displayName;
                resolve(this.suggesterName);
            }, reject);
            // else {
            //     console.log('suggester', this.suggester);
            //     this.suggesterName = 'blank';
            //     resolve('blank');
            // }
        });
    }
}
