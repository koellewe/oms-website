import Topic from "./Topic";

export default class EventSummary {

    constructor(doc){
        this.id = doc.id;
        const data = doc.data();
        this.topic = data.topic;
        this.scheduledUnix = data.scheduled.seconds;
        this.summary = data.summary;
    }

    formatScheduled(detail=false){
        return Topic.formatDate(this.scheduledUnix, detail);
    }

}
