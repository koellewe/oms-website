export default class User {
    constructor(){
        this.uid = null;
        this.email = null;
        this.displayName = null;
        this.role = null;
    }

    populateWithDoc(doc){
        this.uid = doc.id;
        const data = doc.data();
        this.email = data.email;
        this.displayName = data.displayName;
        this.role = data.role;
    }

    populate(id, email, displayName, role){
        this.uid = id;
        this.email = email;
        this.displayName = displayName;
        this.role = role;
    }
}