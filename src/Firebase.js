import env from "./env";
//import firebase from "firebase/app";
import firebase from 'firebase/app';
import 'firebase/app'; //todo
import 'firebase/analytics';
import 'firebase/auth';
import 'firebase/firestore';

// Initialize Firebase
firebase.initializeApp(env.firebaseConf);

export const firestore = firebase.firestore();
export const analytics = firebase.analytics();
