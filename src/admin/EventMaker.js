import React from "react";
import {Paper, TextField, Button} from "@material-ui/core";
import DateFnsUtils from "@date-io/date-fns";
import {KeyboardDateTimePicker, MuiPickersUtilsProvider} from "@material-ui/pickers";
import Snacky from "../ubiquitous/Snacky";
import DoneIcon from '@material-ui/icons/Done';
import {firestore} from "../Firebase";
import ErrorIcon from '@material-ui/icons/Error';

export default class EventMaker extends React.Component {

    constructor(props){
        super(props);

        this.state = {
            inpTopic: '',
            inpDesc: '',
            inpVenue: '',
            selDate: new Date().toLocaleDateString('en-ZA', {
                day: 'numeric', month: 'numeric', year: 'numeric'
            }) +' '+ new Date().toLocaleTimeString('en-ZA', {
                minute: 'numeric', hour: 'numeric'
            }),
            busy: false,
            result: null, snackIcon: null,
        };

        this.onSubmitClick = this.onSubmitClick.bind(this);
    }

    onSubmitClick(){
        if (this.state.inpTopic.length < 3 || this.state.inpDesc.length < 5 || this.state.inpVenue.length < 2){
            this.setState({result: 'Please give more information', snackIcon: (<ErrorIcon/>)});
        }else {
            this.setState({
                busy: true
            });

            firestore.collection('events').add({
                scheduledFor: new Date(this.state.selDate),
                topic: this.state.inpTopic,
                description: this.state.inpDesc,
                venue: this.state.inpVenue,
            }).then(() => {
                this.setState({result: 'Event created!', snackIcon: (<DoneIcon/>), busy: false});
            });
        }
    }

    render() {

        const onTextChange = name => e => {
            this.setState({
                [name]: e.target.value
            });
        };

        const onDateChange = (e, res) => {
            this.setState({
                selDate: res,
            })
        };

        return (<div>
            <Paper style={{padding: '24px'}}>
                <h2>Create an event</h2>
                <TextField label="Topic" fullWidth onChange={onTextChange('inpTopic')}/>
                <TextField label="Description" fullWidth multiline rows="4" onChange={onTextChange('inpDesc')}/>
                <div style={{marginTop: '24px'}}>
                <MuiPickersUtilsProvider utils={DateFnsUtils} >
                    <KeyboardDateTimePicker label="Date and time" format="yyyy-MM-dd HH:mm"
                                            onChange={onDateChange} value={this.state.selDate}/>
                </MuiPickersUtilsProvider></div>
                <TextField label="Venue" onChange={onTextChange('inpVenue')}/>
                <div style={{height: '12px'}}/>
                <Button variant="contained" onClick={this.onSubmitClick} disabled={this.state.busy}
                    color="primary">Submit</Button>
            </Paper>

            <Snacky open={!!this.state.result} onClose={()=>{this.setState({result: null})}}
                icon={this.state.snackIcon} text={this.state.result}/>
        </div>);
    }

}
