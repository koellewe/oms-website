import React from "react";
import {Button, Paper, Table, TableBody, TableCell, TableHead, TableRow, TextField} from "@material-ui/core";
import {firestore} from "../Firebase";
import Spinner from "../ubiquitous/Spinner";
import OmsocEvent from "../data_classes/OmsocEvent";
import {KeyboardDateTimePicker, MuiPickersUtilsProvider} from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import DoneIcon from "@material-ui/core/SvgIcon/SvgIcon";
import Snacky from "../ubiquitous/Snacky";

export default class EventMan extends React.Component {

    constructor(props){
        super(props);

        this.state = {
            events: null,
            selEvent: null,
            inpTopic: '',
            inpDesc: '',
            inpVenue: '',
            selDate: null,
            busy: false,
            result: null, snackIcon: null,
        };

        this.onUpdateClick = this.onUpdateClick.bind(this);
    }

    onUpdateClick(){
        this.setState({busy: true});
        firestore.collection('events').doc(this.state.selEvent).update({
            topic: this.state.inpTopic,
            description: this.state.inpDesc,
            venue: this.state.inpVenue,
            scheduledFor: new Date(this.state.selDate),
        }).then(()=>{
            this.setState({result: 'Event updated!', snackIcon: (<DoneIcon/>), selEvent: null, busy: false});
            this.componentDidMount();
        });
    }

    static frmDate(seconds){
        return new Date(seconds*1000).toLocaleDateString('en-ZA', {
            day: 'numeric', month: 'numeric', year: 'numeric'
        }) +' '+ new Date(seconds*1000).toLocaleTimeString('en-ZA', {
            minute: 'numeric', hour: 'numeric'
        });
    }

    componentDidMount() {
        firestore.collection('events').orderBy('scheduledFor', 'desc').get().then(snap => {
            const events = [];
            snap.forEach(doc => {
                events.push(new OmsocEvent(doc));
            });
            this.setState({events: events});
        });
    }

    render() {

        const onSel = ev => () => {
            this.setState({ // have to copy everything
                selEvent: ev.id,
                inpTopic: ev.topic,
                inpDesc: ev.description,
                inpVenue: ev.venue,
                selDate: EventMan.frmDate(ev.scheduledForUnix),
            });
        };

        const onTextChange = name => e => {
            this.setState({
                [name]: e.target.value
            });
        };

        const onDateChange = (e, res) => {
            this.setState({
                selDate: res,
            })
        };

        return <div>
            <Paper style={{padding: 24}}>
                <h2>Events</h2>
                {!this.state.events &&
                    <Spinner text="Loading events..."/>}
                {this.state.events && <div>
                    <Table>
                        <TableHead><TableRow>
                            <TableCell/>
                            <TableCell>Topic</TableCell>
                            <TableCell>Scheduled</TableCell>
                            <TableCell>Venue</TableCell>
                        </TableRow></TableHead>
                        <TableBody>
                            {this.state.events.map(event => (<TableRow key={event.id}>
                                <TableCell><Button onClick={onSel(event)} variant="outlined">Edit</Button></TableCell>
                                <TableCell>{event.topic}</TableCell>
                                <TableCell>{event.formatScheduledFor(true)}</TableCell>
                                <TableCell>{event.venue}</TableCell>
                            </TableRow>))}
                        </TableBody>
                    </Table>

                    {this.state.selEvent && <div>
                        <h4>Edit Event</h4>
                        <TextField label="Topic" fullWidth value={this.state.inpTopic}
                                   onChange={onTextChange('inpTopic')}/>
                        <TextField label="Description" fullWidth multiline rows="4" value={this.state.inpDesc}
                                   onChange={onTextChange('inpDesc')}/>
                        <div style={{marginTop: '24px'}}>
                            <MuiPickersUtilsProvider utils={DateFnsUtils} >
                                <KeyboardDateTimePicker label="Date and time" format="yyyy-MM-dd HH:mm"
                                                        onChange={onDateChange} value={this.state.selDate}/>
                            </MuiPickersUtilsProvider></div>
                        <TextField label="Venue" onChange={onTextChange('inpVenue')} value={this.state.inpVenue}/>
                        <div style={{height: '12px'}}/>
                        <Button variant="contained" onClick={this.onUpdateClick} disabled={this.state.busy}
                                color="primary">Update</Button>
                    </div>}

                    <Snacky open={!!this.state.result} onClose={()=>{this.setState({result: null})}}
                            icon={this.state.snackIcon} text={this.state.result}/>
                </div>}
            </Paper>
        </div>
    }

}
