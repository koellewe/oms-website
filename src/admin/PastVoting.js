import React from "react";
import {Paper} from "@material-ui/core";
import Spinner from "../ubiquitous/Spinner";
import {firestore} from "../Firebase";
import VotingBallot from "../data_classes/VotingBallot";

export default class PastVoting extends React.Component {

    constructor(props){
        super(props);

        this.state = {
            ballot: null,
        }
    }

    componentDidMount() {
        firestore.collection('voting').where('deadline', '<', new Date())
            .orderBy('deadline', 'desc').limit(1).get().then(snap => {
                let bal = null;
                snap.forEach(doc => {
                    bal = new VotingBallot(doc);
                });
                bal.loadTopics().then(()=>{
                    this.setState({ballot: bal});
                });
        });
    }

    static formatDate(seconds){
        const date = new Date(seconds*1000);
        return date.toLocaleDateString('en-ZA', {
            day: 'numeric', month: 'short', weekday: 'short'
        }) + ' @ ' + date.toLocaleTimeString('en-ZA', {hour12: true, hour:'numeric'});
    }

    render(){
        //some quick setup-maths
        let display = null;
        let totalVotes = 0;
        if (this.state.ballot) {
            this.state.ballot.topics.forEach(topic => {
                totalVotes += topic.votes;
            });
            display = this.state.ballot.topics.map(topic => {
                return {
                    id: topic.id,
                    title: topic.title,
                    perc: totalVotes === 0 ? 0 : Math.round(topic.votes * 100 / totalVotes)
                };
            });
            display.sort((a, b) => {
                return a.perc > b.perc ? -1 : 1;
            });
        }

        return (<Paper style={{padding:'24px'}}>
            <h2>Last voting results</h2>
            {!display && <Spinner text="Loading results..."/>}
            {display && <div>
                <p>Closing date: {PastVoting.formatDate(this.state.ballot.deadlineUnix)}</p>
                <p>Total votes cast: {totalVotes}</p>
                <h4>Topic rankings:</h4>
                <ol>
                    {display.map(top => (<li key={top.id}>
                        {top.title} - {top.perc}%
                    </li>))}
                </ol>

            </div>}
        </Paper>)
    }

}