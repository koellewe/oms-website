import React from "react";
import './Admin.css';
import EmailLister from "./EmailLister";
import {Button} from "@material-ui/core";
import TopicMan from "./TopicMan";
import PastVoting from "./PastVoting";
import EventMaker from "./EventMaker";
import {analytics, /*firestore*/} from "../Firebase";
import EventMan from "./EventMan";
import EventSummaryMan from "./EventSummaryMan";
import AdminMeta from "./AdminMeta";

export default class Admin extends React.Component {

    constructor(props) {
        super(props); //props: loggedInUser

        this.state = {
            display: "DEFAULT", //display-states: DEFAULT, NOTADMIN, NOTLOGGED, [Misc items]
            hecticEnabled: false,
        };

        this.onMenuOptClick = this.onMenuOptClick.bind(this);
    }

    async onHecticClick(){

    }

    componentDidMount() {
        analytics.logEvent('screen_view', {'screen_name': 'Admin'});
    }

    onMenuOptClick(event){
        this.setState({
            display: event.currentTarget.value
        });
    }

    render() {

        const display = this.props.loggedInUser ? this.props.loggedInUser.role === 'admin' ?
            this.state.display : "NOTADMIN" : "NOTLOGGED";

        const menuOpts = [
            ['Subscribers', 'SUBS', <EmailLister/>],
            ['Topics', 'TOPICS', <TopicMan/>],
            ['Last voting', 'LASTVOTE', <PastVoting/>],
            ['Create event', 'NEWEVENT', <EventMaker/>],
            ['Scheduled events', 'EVENTS', <EventMan/>],
            ['New event summary', 'NEWSUMMARY', <EventSummaryMan/>],
            ['Admins', 'METAADMIN', <AdminMeta/>]
        ];

        return (<div id="admin_main">
            <h1>Admin Console</h1>

            {(()=>{switch(display){
                case 'NOTLOGGED':
                    return <p>Please log in to access the console.</p>;
                case 'NOTADMIN':
                    return <p style={{fontSize: '2em'}}>This console is for OMS execs only.</p>;
                case 'DEFAULT':
                    return <div>
                        <h2>Manage:</h2>
                        {menuOpts.map(opt => (<div key={opt[1]}>
                            <Button variant="contained" color="primary" value={opt[1]} onClick={this.onMenuOptClick}
                                style={{marginTop: '12px'}}>{opt[0]}</Button>
                        </div>))}

                        <p style={{marginTop: 36}}>This website is currently paid for and
                            maintained by George Rautenbach. For queries, or to organise ownership transfer, contact him&nbsp;
                            <a href="mailto:jgeorgerautenbach@gmail.com">here</a>.</p>

                        {this.state.hecticEnabled &&
                            <Button variant="contained" color="secondary" style={{marginTop: '24px'}}
                                    onClick={this.onHecticClick}>Hectic</Button>}
                    </div>;
                default:
                    let comp = <code>Unable to display requested resource.</code>;
                    for (let menuOpt of menuOpts) { // display selected component
                        if (menuOpt[1] === display) {
                            comp = menuOpt[2]; break;
                        }
                    }

                    return <div style={{marginBottom: '48px'}}>{comp}
                        <Button variant="outlined" value="DEFAULT" onClick={this.onMenuOptClick} color="inherit"
                                style={{marginTop: '24px'}}>&lt; Menu</Button>
                    </div>;
            }})()}
        </div>);
    }

}
