import React from "react";
import VotingComponent from "./VotingComponent";
import {Paper} from "@material-ui/core";
import {
    BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, PieChart, Pie
} from 'recharts';
import ResponsiveContainer from "recharts/lib/component/ResponsiveContainer";
import FormControl from "@material-ui/core/FormControl";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Radio from "@material-ui/core/Radio";

export default class ResultsComponent extends React.Component {

    constructor(props){
        super(props);
        //props: ballot

        this.state = {
            ballot: props.ballot,
            chartType: 'bar',
        };

        this.onChartChange = this.onChartChange.bind(this);
    }

    onChartChange(event){
        this.setState({
            chartType: event.target.value,
        });
    }

    componentDidMount() { //todo make this page listen for vote count updates
        //lazy refresh
        setTimeout(() => {
            this.state.ballot.loadTopics().then(()=>this.forceUpdate())
        }, 7000);
    }

    render() {
        //some quick setup-maths
        let totalVotes = 0;
        this.state.ballot.topics.forEach(topic=>{
            totalVotes += topic.votes;
        });
        let display = this.state.ballot.topics.map(topic=>{
            return {
                id: topic.id,
                title: topic.title,
                perc: totalVotes === 0 ? 0 : Math.round(topic.votes*100/totalVotes)
            };
        });
        display.sort((a, b)=>{
            return a.perc > b.perc ? -1 : 1;
        });
        let i = 1;
        const dataPoints = display.map(topic=>{
            return {
                perc: topic.perc,
                name: i++,
            };
        });

        return (<div>
            <h2>Voting results so far</h2>
            <p>Voting closes in {VotingComponent.ufTimeDiff(this.state.ballot.deadlineUnix)}.</p>

            <Paper className={"paper"}>
                <FormControl component="fieldset" style={{width: '100%'}}><RadioGroup aria-label="Chart type" name="chkChartType" onChange={this.onChartChange} defaultValue="bar">
                <table width='100%'><tbody><tr>
                    <td align="center"><FormControlLabel control={<Radio/>} label="Bar chart" value="bar"/></td>
                    <td align="center"><FormControlLabel control={<Radio/>} label="Pie chart" value="pie"/></td>
                </tr></tbody></table>
                </RadioGroup></FormControl>

                <div id="chart-cover">

                    {this.state.chartType === 'bar' &&
                    <ResponsiveContainer><BarChart data={dataPoints} >
                            <CartesianGrid strokeDasharray="3 3"/>
                            <XAxis name="Topic number" dataKey="name" />
                            <YAxis/>
                            <Tooltip />
                            <Bar dataKey="perc" fill="purple"/>
                    </BarChart></ResponsiveContainer>}
                    {this.state.chartType === 'pie' &&
                    <ResponsiveContainer><PieChart>
                        <Pie dataKey='perc' data={dataPoints} label fill="purple"/>
                        <Tooltip/>
                    </PieChart></ResponsiveContainer>}

                </div>

                <p style={{textAlign: 'right'}}>Total votes: {totalVotes}</p>
                <p style={{fontWeight: 'bold'}}>Topics:</p>
                <ol>
                    {display.map(topic=>{
                        return <li key={topic.id}>{topic.title} - {topic.perc}%</li>
                    })}
                </ol>
            </Paper>

            <p style={{fontSize: '1.7em', textAlign: 'center'}}>Share this site with others to get more opinions!</p>
        </div>);
    }

}