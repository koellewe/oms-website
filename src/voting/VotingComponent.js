import React from "react";
import FormControl from '@material-ui/core/FormControl';
import {RadioGroup} from "@material-ui/core";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Radio from "@material-ui/core/Radio";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";
import {firestore} from "../Firebase";


export default class VotingComponent extends React.Component {

    constructor(props){
        super(props);
        //props: ballot, loggedInUser, onVote

        this.state = {
            ballot: props.ballot,
            selectedOpt: null
        };

        this.onVoteClick = this.onVoteClick.bind(this);
    }

    static ufTimeDiff(futureTimeSecs){
        const diffMs = new Date(futureTimeSecs*1000) - new Date();
        const diffH = diffMs / (1000 * 60 * 60);
        const days = Math.floor(diffH/24);
        const hours = Math.floor(diffH % 24);
        const dayPart = days === 0 ? '' : days + ' ' + (days===1 ? 'day':'days');
        const hourPart = hours === 0 ? '' : hours + ' ' + (hours===1 ? 'hour':'hours');
        return (dayPart==='' && hourPart==='') ? 'less than an hour' : dayPart + ' ' + hourPart;
    }

    onVoteClick(){
        const self = this;
        firestore.collection('voting/'+this.state.ballot.id+'/votes').doc(this.props.loggedInUser.uid).set({
            voter: this.props.loggedInUser.uid,
            votee: this.state.selectedOpt
        }).then(x=>{
            self.props.onVote();
        }).catch(e=>{ // probably because duplicate vote
            self.props.onVote();
        });
    }

    render() {

        const onRadClick = (event) => {
            this.setState({
                selectedOpt: event.target.value
            });
        };
        const onJustResClick = () => {
            this.props.onVote();
        };

        const buttonLinkStyle = {
            marginTop: '12px',
            color: '#09d3ac',
            textTransform: 'none',
            //fontSize: '.7em'
        };

        return (<div>
            <h2>Vote for your preferred topic of discussion</h2>
            <p>Voting closes in {VotingComponent.ufTimeDiff(this.state.ballot.deadlineUnix)}.</p>

            <Paper className={"paper"}>
                <FormControl component="fieldset">
                    <RadioGroup aria-label="topic" name="radTopic" onChange={onRadClick}>
                        {this.state.ballot.topics.map(topic => {
                            return <FormControlLabel control={<Radio/>} label={topic.title} value={topic.id} key={topic.id}/>
                        })}
                    </RadioGroup>
                </FormControl><br/>
                <Button variant="contained" onClick={this.onVoteClick} color="primary" style={{marginTop: '12px'}}
                        disabled={!this.props.loggedInUser || this.props.loggedInUser.role !== 'uctcom' || this.state.selectedOpt===null}>
                    {this.props.loggedInUser ? this.props.loggedInUser.role === 'uctcom' ?
                        'Vote' : 'Voting only for UCT' : 'Login to vote'}
                </Button><br/>
                <Button style={buttonLinkStyle} variant="text" onClick={onJustResClick}
                        className={"link-button"}><u>Just see results</u></Button>
            </Paper>
        </div>);
    }

}